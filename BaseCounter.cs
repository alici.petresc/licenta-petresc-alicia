using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCounter : MonoBehaviour, IKitchenObjectParent
{
    [SerializeField] private Transform counterTopPoint;

    private KitchenObject kitchenObject;

    public virtual void Interact(Player player)
    {
        Debug.LogError("BaseCounter.Interact();");
    }

    public virtual void InteractAlternate(Player player)
    {
        Debug.LogError("BaseCounter.InteractAlternate();");
    }


    // the IKitchenObjectParent interface

    public Transform GetKitchenObjectFollowTransform()   // spawn object on the counter top point that was set
    {
        return counterTopPoint;
    }

    public void SetKitchenObject(KitchenObject kitchenObject)  // set
    {
        this.kitchenObject = kitchenObject;
    }

    public KitchenObject GetKitchenObject()    // get
    {
        return kitchenObject;
    }

    public void ClearKitchenObject()   // if there is no object on the Counter
    {
        kitchenObject = null;
    }

    public bool HasKitchenObject()     // if there is an object on the Counter
    {
        return kitchenObject != null;
    }

}
