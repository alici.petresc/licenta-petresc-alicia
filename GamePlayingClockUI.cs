using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayingClockUI : MonoBehaviour
{
    [SerializeField] private Image timerImage;    // reference to image

    private void Update()
    {
        timerImage.fillAmount = GameHandler.Instance.GetGamePlayingTimerNormalized();
    }
}
