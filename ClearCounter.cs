using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearCounter : BaseCounter
{
    [SerializeField] private KitchenObjectSO kitchenObjectSO;
  

    public override void Interact(Player player)   // overrides the method from BaseCounter, if the Counter is not a base type
    {
       if (!HasKitchenObject())
        {   // There is NO KitchenObject here
            if(player.HasKitchenObject())  // Player is carrying an object
            {
                player.GetKitchenObject().SetKitchenObjectParent(this);   // get the object from player and change its parent to counter 
            } else  
            {
                // Player is not carrying an object
            }
        } else   
        {  // There is a KitchenObject here
            if(player.HasKitchenObject())
            {
                // Player is carrying something
                if(player.GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject ))
                {
                    if (plateKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO()))
                    { // Player is holding a plate
                        GetKitchenObject().DestroySelf();
                    }
                } else  
                { // player is not carrying a plate, but an ingredient
                    if(GetKitchenObject().TryGetPlate(out plateKitchenObject))
                    { // Counter is holding a plate
                        if (plateKitchenObject.TryAddIngredient(player.GetKitchenObject().GetKitchenObjectSO()))
                        {
                            player.GetKitchenObject().DestroySelf();
                        }
                    }
                }
            } else
            {
                // Player is not carring anything
                GetKitchenObject().SetKitchenObjectParent(player);
            }
        }
    }  
}
