using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingCounter : BaseCounter
{
    public event EventHandler <OnProgressChangedEventArgs> OnProgressChanged;  // track progress for cutting (progress bar)
    public class OnProgressChangedEventArgs : EventArgs
    {
        public float progressNormalized;
    }

    public event EventHandler OnCut;

    [SerializeField] private CuttingRecipeSO[] cuttingRecipeSOArray;

    private int cuttingProgress;

    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {   // There is NO KitchenObject here
            if (player.HasKitchenObject())  // Player is carrying an object
            {
                if(HasRecipeWithInput(player.GetKitchenObject().GetKitchenObjectSO()))
                    // Player carring an object that can be cut
                player.GetKitchenObject().SetKitchenObjectParent(this);   // get the object from player and change its parent to counter 
                cuttingProgress = 0;

                CuttingRecipeSO cuttingRecipeSO = GetCuttingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());

                // Firing event for cutting progress
                OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs   // firing event for cutting
                {
                progressNormalized = (float) cuttingProgress / cuttingRecipeSO.cuttingProgressMax 
                }) ;

            }
            else
            {
                // Player is not carrying an object
            }
        }
        else
        {  // There is a KitchenObject here
            if (player.HasKitchenObject())
            {
                // Player is carrying something
                if (player.GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject))
                {
                    if (plateKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO()))
                    { // Player is holding a plate
                        GetKitchenObject().DestroySelf();
                    }
                }
            }
            else
            {
                // Player is not carring anything
                GetKitchenObject().SetKitchenObjectParent(player);
            }
        }
    }

    public override void InteractAlternate (Player player)
    {
        if (HasKitchenObject() && HasRecipeWithInput(GetKitchenObject().GetKitchenObjectSO()))
        {
            //There is a kitchen object here and object can be cut
            cuttingProgress++;

            OnCut?.Invoke(this, EventArgs.Empty);
            CuttingRecipeSO cuttingRecipeSO = GetCuttingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());

            // Firing event for cutting progress
            OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs   // firing event for cutting
            {
                progressNormalized = (float)cuttingProgress / cuttingRecipeSO.cuttingProgressMax
            });

            if (cuttingProgress >= cuttingRecipeSO.cuttingProgressMax)
            {
                KitchenObjectSO outputKitchenObjectSO = GetOutputForInput(GetKitchenObject().GetKitchenObjectSO());
                GetKitchenObject().DestroySelf();  //distroys the prefab

                KitchenObject.SpawnKitchenObject(outputKitchenObjectSO, this);   // spawns the cut version of vegetables
            }
        }
    }

    private bool HasRecipeWithInput(KitchenObjectSO inputKitchenObjectSO)  // check if input matches the added recipes
    {
        CuttingRecipeSO cuttingRecipeSO = GetCuttingRecipeSOWithInput(inputKitchenObjectSO);
        return cuttingRecipeSO != null;
    }

    private KitchenObjectSO GetOutputForInput (KitchenObjectSO inputKitchenObjectSO)
    { 
        CuttingRecipeSO cuttingRecipeSO= GetCuttingRecipeSOWithInput(inputKitchenObjectSO);

        if (cuttingRecipeSO != null)    // if the entered input matches the selected object return the entered output
        {
            return cuttingRecipeSO.output;
        }
        else
        {
            return null;
        }
    }

    private CuttingRecipeSO GetCuttingRecipeSOWithInput (KitchenObjectSO inputKitchenObjectSO)
    {
        foreach (CuttingRecipeSO cuttingRecipeSO in cuttingRecipeSOArray)
        {
            if (cuttingRecipeSO.input == inputKitchenObjectSO)    // if the entered input matches the selected object return the entered output
            {
                return cuttingRecipeSO;
            }
        }
        return null;
    }
}
