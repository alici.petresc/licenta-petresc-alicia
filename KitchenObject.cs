using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenObject : MonoBehaviour
{

    [SerializeField] private KitchenObjectSO kitchenObjectSO;

    private IKitchenObjectParent kitchenObjectParent;

    public KitchenObjectSO GetKitchenObjectSO()
    {
        return kitchenObjectSO;
    }

    public void SetKitchenObjectParent ( IKitchenObjectParent kitchenObjectParent)
    {
        if(this.kitchenObjectParent != null)  // if there is an object on the Counter
        {
            this.kitchenObjectParent.ClearKitchenObject();  //clear current parent Counter
        }

        this.kitchenObjectParent = kitchenObjectParent;  // old parent -> new parent

        if(kitchenObjectParent.HasKitchenObject())
        {
            Debug.LogError("IKitchenObjectParent already has a KitchenObject!");
        }

        kitchenObjectParent.SetKitchenObject(this);  // transfer an object from one Counter to another Counter 

        transform.parent = kitchenObjectParent.GetKitchenObjectFollowTransform();  
        transform.localPosition = Vector3.zero;
    }

    public IKitchenObjectParent GetKitchenObjectParent()
    {
        return kitchenObjectParent;
    }

    public void DestroySelf()
    {
        kitchenObjectParent.ClearKitchenObject();
        Destroy(gameObject);
    }
    
    public bool TryGetPlate( out PlateKitchenObject plateKitchenObject)
    {
        if(this is PlateKitchenObject)
        {
            plateKitchenObject = this as PlateKitchenObject;
            return true;
        } else
        {
            plateKitchenObject = null;
            return false;
        }

    }

    public static KitchenObject SpawnKitchenObject( KitchenObjectSO kitchenObjectSO, IKitchenObjectParent kitchenObjectParent)   
        // made it a static function in order to belong to the class itself (as opposed to any instances)
    {
        Transform kitchenObjectTransform = Instantiate(kitchenObjectSO.prefab);
        KitchenObject kitchenObject = kitchenObjectTransform.GetComponent<KitchenObject>();
        
        kitchenObject.SetKitchenObjectParent(kitchenObjectParent); // spawn cut element on counter

        return kitchenObject;
    }
}